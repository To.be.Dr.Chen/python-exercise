import random

def initialize():
    """
    Diese Funktion soll die Anfangskonfiguration des Spiels herstellen.
    Rueckgabewerte dieser Funktion sind fuer die auf dem Aufgabenblatt vorgeschlagene Datenstruktur 
    das Dictionary stacks, die Liste stack2deal und die Liste cards2deal_perstack.
    """
    # TODO: Hier kommt ihr Code
    ##stack2deal
    deck_of_cards = []
    deck_of_cards = [(value, color) for value in range(1, 14) for color in ['hearts', 'spades']]  ##这个语句就很方便了
    ##print(deck_of_cards)
    stack2deal = 4 * deck_of_cards
    random.shuffle(stack2deal)

    ##stacks
    stacks = {k: [stack2deal.pop()] for k in range(1, 11)}

    ##cards2deal_perstack
    cards2deal_perstack = [50, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4]

    return stacks, stack2deal, cards2deal_perstack


def deal(stacks, stack2deal, cards2deal_perstack):
    """
    Funktion, die das Austeilen realisiert.
    """
    # TODO: Hier kommt ihr Code
    for key in range(1,11):
        print(stacks[key])
        stacks[key].append(stack2deal.pop(0))
        test_fullsequence(key, stacks, stack2deal, cards2deal_perstack)
    cards2deal_perstack[0] -=10
    return stacks, stack2deal, cards2deal_perstack

def test_fullsequence(key, stacks, stack2deal, cards2deal_perstack):
    """
    Funktion, die fuer einen bestimmten Stapel prueft, ob am Ende dieses Stapels eine Sequenz
    von Koenig bis Ass vorliegt und diese vollstaendige Sequenz dann entfernt.
    Verwenden Sie diese Funktion auch an den richtigen Stellen der Funktion `deal`.
    """
    # TODO: Hier kommt ihr Code
    deck_of_heart = [(value, 'hearts') for value in range(1, 14) ]
    deck_of_spades = [(value, 'spades') for value in range(1, 14)]
    deck_of_heart = list(reversed(deck_of_heart))
    deck_of_spades = list(reversed(deck_of_spades))
    lenk = len(stacks[key])
    if lenk >= 13:
        if stacks[key][lenk-13:lenk] == deck_of_spades or stacks[key] == deck_of_heart:
            for i in range(13):
                stacks[key].pop()
        test_revealcard(key, stacks, stack2deal, cards2deal_perstack)
    return stacks, stack2deal, cards2deal_perstack
def test_revealcard(key, stacks, stack2deal, cards2deal_perstack):
    """
    Prueft fuer einen bestimmten Stapel, ob am Ende dieses Stapels keine offene,
    sondern noch eine aufzudeckende Karte liegt und diese dann aufdeckt.
    Verwenden Sie diese Funktion auch an den richtigen Stellen der Funktion `test_fullsequence`.
    """
    # Diese Funktion ist bereits implementiert, sie wurde im Video bereits gezeigt. 
    if cards2deal_perstack[key] > 0 and not stacks[key]:
        stacks[key].append([stack2deal.pop()])
        cards2deal_perstack[key] -= 1
        return stacks, stack2deal, cards2deal_perstack
    else:
        pass


stacks, stack2deal, cards2deal_perstack = initialize()  
print('zhanshi:',stacks)
print('suoyou:',stack2deal)
print(cards2deal_perstack)
deal(stacks, stack2deal, cards2deal_perstack)