import time
from concurrent import futures


def test(t):
    time.sleep(t)
    print("Ich habe {} Sekunden gewartet. Zeit {:.0f}".format(t, time.time()))
print("Startzeit:               {:.0f}".format(time.time()))

if __name__ == '__main__':

    with futures.ProcessPoolExecutor(max_workers=3) as ex:
        ex.submit(test,9)
        ex.submit(test, 4)
        ex.submit(test, 5)
        ex.submit(test, 6)
        print("Alle Aufgaben gestartet.")
    print("Alle Aufgaben erledigt.")


##Bei Process wird "Startzeit:" wiederholt
## Bei Threads laeuft es ein Prozess.
## Bei Processpoll laeuft es 4 Prozesse.