import time
from concurrent import futures
#from concurrent.futures import ThreadPoolExecutor, as_completed

def is_prime(n):
    i = 2
    while i*i <= n:
        # n ist durch i teilbar -> n ist nicht prim
        if n % i == 0:
            return False
        i += 1

    # keinen Teiler gefunden -> n ist prim
    return True


zahlen = [
    32005250866063,
    8220988541723,
    31727681049289,
    8220988541809,
    31727681049289,
    6034278144347,
    26461391127337,
    3717886348249
    ]
if __name__ == '__main__':
    pool = futures.ProcessPoolExecutor(3)
    start = time.perf_counter()
    R_list=[]
    #print(type(R))
    #for i in zahlen:
    #    R.append(pool.submit(is_prime,i))
    R = pool.map(is_prime,zahlen)
    print(R)
    for results in R:
        print(results)
    '''
    for future in as_completed(R):
        data = future.result()
        R_list.append(data)
    dic = {zahlen[i]:R_list[i] for i in range(len(zahlen))}
    print(dic)
    '''
    #for i in range(len(zahlen)):
    #    print('Die Aussage, Zahl {} Primezahl ist, ist {}'.format(zahlen[i],R_list[i]))
    pool.shutdown(wait=True)
    stop=time.perf_counter()
    print('\nThe duration is',stop-start)
'''
Die Laufzeit ist deutlich weniger. 
'''
