import time
from concurrent.futures import ThreadPoolExecutor, as_completed

def is_prime(n):
    i = 2
    while i*i <= n:
        # n ist durch i teilbar -> n ist nicht prim
        if n % i == 0:
            return False
        i += 1

    # keinen Teiler gefunden -> n ist prim
    return True


zahlen = [
    32005250866063,
    8220988541723,
    31727681049289,
    8220988541809,
    31727681049289,
    6034278144347,
    26461391127337,
    3717886348249
    ]
pool = ThreadPoolExecutor(3)
start = time.time()
R_list=[]
#print(type(R))
#for i in zahlen:
#    R.append(pool.submit(is_prime,i))
R = [pool.submit(is_prime,i) for i in zahlen]
print(R)


for future in as_completed(R):
    data = future.result()
    R_list.append(data)
dic = {zahlen[i]:R_list[i] for i in range(len(zahlen))}
print(dic)
#for i in range(len(zahlen)):
#    print('Die Aussage, Zahl {} Primezahl ist, ist {}'.format(zahlen[i],R_list[i]))
pool.shutdown(wait=True)
stop=time.time()
print(stop-start)
'''
Nachteil von Result:Result trift bei Durchfuefung.
Das Fehlen einer einheitlichen Verwaltung von Threads kann ohne Einschränkungen neue Threads erstellen, 
miteinander konkurrieren und zu viele Systemressourcen belegen, um Abstürze oder Ooms zu verursachen.
Fehlen weiterer Funktionen wie reguläre Ausführung, reguläre Ausführung, Thread-Unterbrechung.

Vorteil von as_competed:
as_completed can get the result only after the tasks are finished
'''
