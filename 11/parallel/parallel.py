import time
from concurrent import futures

def test(t):
    time.sleep(t)
    print("Ich habe {} Sekunden gewartet. Zeit {:.0f}".format(t, time.time()))


print("Startzeit:                         {:.0f}".format(time.time()))
if __name__ == '__main__':
    with futures.ThreadPoolExecutor(max_workers=3) as ex: ## Hier werden 3 Threads in Threads estabiliert.
        ##with futures.ThreadPoolExecutor(max_workers=4) as ex: disese 4 Taske werden gleichzeitig angefangen.
        ex.submit(test, 9)##submit the test task, parameter is 9.
        ex.submit(test, 4)## Erster geschaffen in 4 Sekunden,dann (test,6) einzufuegen.
        ex.submit(test, 5)## Diese 3 sind gleichzeitig durchgeführt
        ex.submit(test, 6)##geschaffen in 10(4+6) Sekunde

        print("Alle Aufgaben gestartet.")

print("Alle Aufgaben erledigt.")