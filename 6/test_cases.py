from spidersolitaire import Card, Sequence, Stack, SpiderSolitaire
import unittest
import types
import itertools


def skip_not_impl(obj, *attr):
    c_obj = obj
    # recursively check if 'attr' exists in 'obj'
    for name in attr:
        if not hasattr(c_obj, name):
            break
        c_obj = getattr(c_obj, name)
    else:
        if type(c_obj) == types.FunctionType:
            return lambda f: f
    
    return unittest.skip("{!r} is not yet implemented.".format(".".join(attr)))

def call_counter(func):
    count = 0

    def f(*args, **kwargs):
        nonlocal count
        count += 1
        return func(*args, *kwargs)

    def get_count():
        return count

    return f, get_count


class CardTests(unittest.TestCase):
    @skip_not_impl(Card, "__init__")
    def test_a(self):
        "'Konstruktor' - Test"
        for color, value in itertools.product(["hearts", "spades"], range(1, 14)):
            card = Card(value, color)
            self.assertEqual(card._value, value)
            self.assertEqual(card._color, color)
    
    @skip_not_impl(Card, "get_value")
    def test_b_i(self):
        "'get_value' - Test"
        for color, value in itertools.product(["hearts", "spades"], range(1, 14)):
            card = Card(value, color)
            self.assertEqual(card.get_value(), value)
    
    @skip_not_impl(Card, "get_color")
    def test_b_ii(self):
        "'get_color' - Test"
        for color, value in itertools.product(["hearts", "spades"], range(1, 14)):
            card = Card(value, color)
            self.assertEqual(card.get_color(), color)
    
    @skip_not_impl(Card, "fits_to")
    def test_c(self):
        "'fits_to' - Test"
        card1 = Card(6, "hearts")
        card2 = Card(7, "hearts")
        card3 = Card(7, "spades")
        
        self.assertEqual(card1.fits_to(card2), True)
        self.assertEqual(card1.fits_to(card2, value_only=False), True)
        self.assertEqual(card1.fits_to(card2, value_only=True), False)
        
        self.assertEqual(card1.fits_to(card3), False)
        self.assertEqual(card1.fits_to(card3, value_only=False), False)
        self.assertEqual(card1.fits_to(card3, value_only=True), True)
        
        self.assertEqual(card2.fits_to(card3), False)
        self.assertEqual(card2.fits_to(card3, value_only=False), False)
        self.assertEqual(card2.fits_to(card3, value_only=True), False)


class SequenceTests(unittest.TestCase):
    @skip_not_impl(Sequence, "__init__")
    def test_a(self):
        "'Konstruktor' - Test"
        lst = [Card(2, "hearts"), Card(1, "hearts")]
        seq = Sequence(list(lst))
        self.assertEqual(seq._cards, lst)

    @skip_not_impl(Sequence, "first_card")
    def test_b(self):
        "'first_card' - Test"
        first = Card(1, "hearts")
        seq = Sequence([first,Card(2, "hearts")])
        self.assertEqual(seq.first_card(), first)

    @skip_not_impl(Sequence, "last_card")
    def test_b(self):
        "'last_card' - Test"
        last = Card(1, "hearts")
        seq = Sequence([Card(2, "hearts"), last])
        self.assertEqual(seq.last_card(), last)
    
    @skip_not_impl(Sequence, "append_card")
    def test_c(self):
        "'append_card' - Test"
        lst = [Card(2, "hearts"), Card(1, "hearts")]
        seq = Sequence(lst[:1])

        seq.append_card(lst[-1])
        self.assertEqual(seq._cards, lst)
    
    @skip_not_impl(Sequence, "fits_to")
    def test_d(self):
        "'fits_to' - Test"
        seq1 = Sequence([Card(2, "hearts"), Card(1, "hearts")])
        seq2 = Sequence([Card(4, "hearts"), Card(3, "hearts")])
        seq3 = Sequence([Card(4, "spades"), Card(3, "spades")])
        
        self.assertEqual(seq1.fits_to(seq2), True)
        self.assertEqual(seq1.fits_to(seq2, value_only=False), True)
        self.assertEqual(seq1.fits_to(seq3), False)
        self.assertEqual(seq1.fits_to(seq3, value_only=False), False)
        
        self.assertEqual(seq1.fits_to(seq2, value_only=True), False)
        self.assertEqual(seq1.fits_to(seq3, value_only=True), True)
        
        self.assertEqual(seq2.fits_to(seq1), False)
        self.assertEqual(seq2.fits_to(seq1, value_only=False), False)
        self.assertEqual(seq2.fits_to(seq1, value_only=True), False)
    
    @skip_not_impl(Sequence, "merge")
    def test_e(self):
        "'merge' - Test"
        lst = [Card(4, "hearts"), Card(3, "hearts"), Card(2, "hearts"), Card(1, "hearts")]
        seq1 = Sequence(lst[2:])
        seq2 = Sequence(lst[:2])
        #seq2 = Sequence(lst)
        seq2.merge(seq1)
        self.assertEqual(seq2._cards, lst)
    
    @skip_not_impl(Sequence, "split")
    def test_f(self):
        "'split' - Test"
        lst = [Card(4, "hearts"), Card(3, "hearts"), Card(2, "hearts"), Card(1, "hearts")]
        seq = Sequence(list(lst))

        seq2 = seq.split(2)
        self.assertIsInstance(seq2, Sequence, msg="Der Rueckgabewert von 'sequence.split' soll eine neues 'Sequence'-Objekt sein")
        self.assertEqual(seq2._cards, lst[2:])
        self.assertEqual(seq._cards, lst[:2])
    
    @skip_not_impl(Sequence, "is_full")
    def test_g(self):
        "'is_full' - Test"
        lst1 = [Card(v, "hearts") for v in range(13, 0, -1)]
        lst2 = [Card(v, "spades") for v in range(13, 0, -1)]

        self.assertEqual(Sequence(list(lst1)).is_full(), True)
        self.assertEqual(Sequence(lst1[:-1]).is_full(), False)
        self.assertEqual(Sequence(lst1[1:]).is_full(), False)
        self.assertEqual(Sequence(list(lst2)).is_full(), True)
        self.assertEqual(Sequence(lst2[:-1]).is_full(), False)
        self.assertEqual(Sequence(lst2[3:]).is_full(), False)


class StackTests(unittest.TestCase):
    @skip_not_impl(Stack, "__init__")
    def test_a(self):
        "'Konstruktor' - Test"
        card = Card(1, "hearts")
        down = [Card(v, "spades") for v in range(2, 5)]
        stack = Stack(card, list(down))
        
        self.assertIsInstance(stack._sequences, list, msg="'stack._sequences' soll eine Liste sein.")
        self.assertEqual(len(stack._sequences), 1, msg="'stack._sequences' sollte nach dem Konstruktor ein 'Sequence'-Objekt beinhalten.")
        self.assertIsInstance(stack._sequences[0], Sequence, msg="'stack._sequences' sollte nach dem Konstruktor ein 'Sequence'-Objekt beinhalten.")
        self.assertEqual(len(stack._sequences[0]._cards), 1, msg="Die Sequenz soll nur die uebergebene Karte beinhalten.")
        self.assertEqual(stack._sequences[0].first_card(), card)
        self.assertEqual(stack._face_down_cards, down)
        
    @skip_not_impl(Stack, "last_sequence")
    def test_b(self):
        "'last_sequence' - Test"
        stack = Stack(Card(2, "spades"), [])
        
        seq = Sequence([Card(1, "hearts")])
        stack._sequences.append(seq)
        stack._sequences.insert(0, Sequence([Card(3, "hearts")]))
        print(seq)
        self.assertEqual(stack.last_sequence(), seq, msg="'stack.last_sequence' soll die letzte Sequenz in 'stack._sequences' zurueckgeben.")
        
    @skip_not_impl(Stack, "is_empty")
    def test_c(self):
        "'is_empty' - Test"
        stack = Stack(Card(1, "hearts"), [])
        
        self.assertEqual(stack.is_empty(), False)
        stack._sequences.clear()
        self.assertEqual(stack.is_empty(), True)
        
    @skip_not_impl(Stack, "append_sequence")
    def test_d(self):
        "'append_sequence' - Test"
        stack = Stack(Card(2, "hearts"), [])
        seq = Sequence([Card(1, "spades")])
        
        seq1 = stack.last_sequence()
        stack.append_sequence(seq)

        self.assertEqual(stack._sequences, [seq1, seq], msg="Die uebergebene Sequenz soll an 'stack._sequences' angehaengt werden.")
        
    @skip_not_impl(Stack, "test_fullsequence")
    def test_e(self):
        "'test_fullsequence' - Test"
        full = [Card(v, "hearts") for v in range(13, 0, -1)]
        down = [Card(v, "spades") for v in range(5, 0, -1)]
        
        # Case 1: The sequence is full
        stack = Stack(full[0], list(down))
        seq = stack.last_sequence()
        seq.merge(Sequence(full[1:]))
        
        seq.is_full, get_is_full_count = call_counter(seq.is_full)
        stack.test_revealcard, get_revealcard_count = call_counter(stack.test_revealcard)
        
        stack.test_fullsequence()
        
        self.assertNotIn(seq, stack._sequences, msg="Bei voller Sequenz soll die Sequenz aus 'stack._sequences' entfernt werden.")
        # check if Sequence.is_full was called
        self.assertEqual(get_is_full_count(), 1, msg="In 'stack.test_fullsequence' soll 'sequence.is_full' von der letzten Sequenz          verwendet werden.")
        # check if Stack.test_revealcard was called
        self.assertEqual(get_revealcard_count(), 1, msg="Wenn eine volle Sequenz in 'stack.test_fullsequence' entfernt wurde soll 'Stack.test_revealcard' aufgerufen werden.")
        
        # Case 2: The sequence is not full
        stack = Stack(full[0], list(down))
        seq = stack.last_sequence()
        
        seq.is_full, get_is_full_count = call_counter(seq.is_full)
        stack.test_revealcard, get_revealcard_count = call_counter(stack.test_revealcard)
        
        stack.test_fullsequence()
        
        self.assertIn(seq, stack._sequences, msg="Bei nicht-voller Sequenz soll der Stapel unverändert bleiben.")
        # check if Sequence.is_full was called
        self.assertEqual(get_is_full_count(), 1, msg="In 'stack.test_fullsequence' soll 'sequence.is_full' von der letzten Sequenz          verwendet werden.")
        
    @skip_not_impl(Stack, "deal_card")
    def test_f(self):
        "'deal_card' - Test"
        card = Card(10, "hearts")
        
        # Case 1: Card fits to the last sequence
        stack = Stack(card, [])
        card1 = Card(9, "hearts")

        stack.test_fullsequence, get_fullsequence_count = call_counter(stack.test_fullsequence)
        stack.deal_card(card1)
        
        self.assertEqual(len(stack._sequences), 1, msg="Bei passender Karte soll der Stapel immer noch nur 1 Sequenz beinhalten.")
        self.assertEqual(stack.last_sequence()._cards, [card, card1], msg="Bei passender Karte soll der letzte Stapel zusätzlich die ausgeteilte Karte beinhalten.")
        self.assertEqual(get_fullsequence_count(), 1, msg="Bei passender Karte soll 'stack.test_fullsequence' aufgerufen werden.")
        
        # Case 2: Card doesn't fit to the last sequence
        stack = Stack(card, [])
        card1 = Card(9, "spades")

        stack.test_fullsequence, get_fullsequence_count = call_counter(stack.test_fullsequence)
        stack.deal_card(card1)

        self.assertEqual(len(stack._sequences), 2, msg="Bei nicht-passender Karte soll der Stapel aus 2 Sequenzen bestehen.")
        self.assertEqual(stack._sequences[0]._cards, [card], msg="Bei nicht-passender Karte soll die ursprüngliche Sequenz nicht verändert werden.")
        self.assertEqual(stack.last_sequence()._cards, [card1], msg="Bei nicht-passender Karte soll der letzte Stapel (nur) die ausgeteilte Karte beinhalten.")
        self.assertEqual(get_fullsequence_count(), 0, msg="Bei nicht-passender Karte muss 'stack.test_fullsequence' nicht aufgerufen werden.")


class SSTests(unittest.TestCase):
    @skip_not_impl(SpiderSolitaire, "deal")
    def test_a(self):
        "'deal' - Test"
        ss = SpiderSolitaire()
        
        get_dealcard_counts = []

        for stack in ss._stacks:
            stack.deal_card, get_dealcard_count = call_counter(stack.deal_card)
            get_dealcard_counts.append(get_dealcard_count)
        
        ss.deal()

        # Case 1: We can deal the cards
        self.assertEqual(len(ss._stack2deal), 40, msg="Es müssen insgesamt 10 Karten verteilt werden")
        self.assertEqual([f() for f in get_dealcard_counts], [1] * len(ss._stacks), msg="Auf jedem Stapel soll genau 1 Mal 'deal_card' aufgerufen werden")
        
        # Case 2: There is an empty stack
        ss._stacks[5]._sequences.clear()
        ss.deal()
        
        self.assertEqual(len(ss._stack2deal), 40, msg="Wenn es einen leeren Stapel gibt, darf nicht ausgeteilt werden.")
        
        # Case 3: no Cards to deal
        ss._stack2deal.clear()
        ss._stacks[5].append_sequence(Sequence([Card(5, "hearts")]))
        ss.deal()



class Result(unittest.TextTestResult):
    def getDescription(self, test):
        doc_first_line = test.shortDescription()
        if self.descriptions and doc_first_line:
            return doc_first_line
        else:
            return str(test)

    def addSkip(self, test, reason):
        unittest.TestResult.addSkip(self, test, reason)
        if self.showAll:
            self.stream.writeln("skipped ({})".format(reason))
        elif self.dots:
            self.stream.write("s")
            self.stream.flush()
    
    def wasSuccessful(self):
        return len(self.failures) == len(self.errors) == len(self.skipped) == 0


_failed = False
def run_test(test, msg):
    global _failed
    if _failed:
        return

    suite = unittest.defaultTestLoader.loadTestsFromTestCase(test)
    l = len(msg) + 8
    line = "+" + "-" * l + "+"
    print(line + "\n|    " + msg + "    |\n" + line)
    res = unittest.TextTestRunner(verbosity=2, failfast=True, resultclass=Result).run(suite)
    if not res.wasSuccessful():
        _failed = True


if __name__ == "__main__":
    run_test(CardTests, "Testing class 'Card'")
    
    print()
    run_test(SequenceTests, "Testing class 'Sequence'")
    
    print()
    run_test(StackTests, "Testing class 'Stack'")
    
    print()
    run_test(SSTests, "Testing class 'SpiderSolitaire'")
