import random



# Dieses dictionary dient lediglich zur Ausgabe in der Console.
# Es wird in der Klasse Card und Stack für die magische Methode '__str__' verwendet
n = [i for i in range(14) if i != 11]
uni_cards = {
    'hearts': {v + 1: chr(127153 + n[v]) for v in range(13)},
    'spades': {v + 1: chr(127137 + n[v]) for v in range(13)},
    'face_down': chr(127136)
}


class Card:
    """
    Eine einzelne Spielkarte, die Informationen bzgl Kartenwert und -Farbe speichert.
    """

    def __init__(self, value, color):

        self._value = value
        self._color = color

    # TODO: Hier kommt
    #  Ihr Code
    def get_value(self):
        return self._value

    def get_color(self):
        return self._color

    #@staticmethod
    def fits_to(self,card, value_only=False):
        a=self._value == (card.get_value() - 1) and self._color == card.get_color()

        if value_only == False :
            if a == True:
                value_only = True
            else:
                value_only = False
        else:
            if self._value == (card.get_value() - 1) and self._color != card.get_color():
                value_only = True
            else:
                value_only = False
        return value_only

    def __str__(self):
        return uni_cards[self.get_color()][self.get_value()]


class Sequence:
    """
    Diese Klasse modelliert eine absteigende Sequenz von Karten
    """
    def __init__(self, card):
        self._cards = card


    def first_card(self):
        "Liefert die erste Karte dieser Sequenz"
        return self._cards[0]

    # TODO: Hier kommt Ihr Code
    def last_card(self):
        return self._cards[-1]

    def append_card(self,lst):
        self._cards.append(lst)
        return self._cards

    def fits_to(self,Seq,value_only=False):
        a = self.first_card()._value == (Seq.last_card()._value-1) and self.first_card()._color == Seq.last_card()._color
        b = self.first_card()._value == (Seq.last_card()._value-1) and self.first_card()._color != Seq.last_card()._color

        if value_only == False:
            if a == True:
                value_only = True
            else:
                value_only = False
        else:
            if b:
                value_only=True
            else:
                value_only=False

        return value_only

    def merge(self,lst):

        #self._cards.append(lst[0])
        #print(self._cards)
        self._cards+=lst._cards

        return self._cards

    def split(self,num):

        l = len(self._cards) - num
        new = []
        for i in range(l):
            a = self._cards.pop(num)
            new.append(a)
        seq1 = Sequence(new)
        return seq1


    def is_full(self):
        a=[Card(v, "hearts") for v in range(13, 0, -1)]
        b=[Card(v, "spades") for v in range(13, 0, -1)]
        if len(self._cards)!=len(a):
            cc = False
        else:
            for i in range(len(a)):
                if ((a[i].get_value()==self._cards[i].get_value() and a[i].get_color()==self._cards[i].get_color())):
                    aa =True
                else:
                    aa=False
                if((b[i].get_value()==self._cards[i].get_value() and b[i].get_color()==self._cards[i].get_color())):
                    bb= True
                else:
                    bb=False
            cc = bb or aa


        return cc

    def __str__(self):
        return "-".join(map(str, self._cards))


class Stack:
    """
    Ein Stapel von Sequenzen. Diese Klasse modelliert die einzelnen Stapel des Spiels.
    Neben den Sequenzen, welche den aufgedeckten Karten entsprechen, merkt sich ein Stapel noch die umgedrehten/verdeckten Karten.
    """

    def __init__(self, sequences,face_down_cards):
        sequences = [sequences]
        self._sequences = [Sequence(list(sequences))]
        self._face_down_cards = face_down_cards

    def is_empty(self):
        "Prueft, ob dieser Stapel leer ist, es also keine offenen Karten mehr gibt."
        return not self._sequences

    def test_revealcard(self):
        """
        Deckt, wenn moeglich, eine neue Karte von den zugedeckten Karten auf.
        Dafuer muss der Stapel leer sein und es muss noch zugedeckte geben.
        """
        if self.is_empty() and self._face_down_cards:
            self.append_sequence(Sequence([self._face_down_cards.pop()]))

    # TODO: Hier kommt Ihr Code


    def last_sequence(self):
        return self._sequences[-1]

    def append_sequence(self,lst):
        self._sequences.append(lst)
        return self._sequences

    def test_fullsequence(self):

        pass

    def deal_card(self):
        pass

    def __str__(self):
        stack_str = " ".join(len(self._face_down_cards) * uni_cards['face_down']) + " "
        stack_str += " ".join(map(str, self._sequences))
        return stack_str


class SpiderSolitaire:
    """
    Klasse, die das ganze Spielfeld an sich verwaltet.
    """

    def __init__(self):
        # wir starten mit allen Karten (4 ganze Kartendecks mit jeweils 13 Herz und 13 Pik)
        self._stack2deal = 4 * [Card(value, color) for value in range(1, 14) for color in ["hearts", "spades"]]
        # Durchmischen aller Karten
        random.shuffle(self._stack2deal)

        # Anzahl verdeckter Karten pro Stapel
        cards2deal_perstack = [5, 5, 5, 5, 4, 4, 4, 4, 4, 4]

        # Es werden 10 Stapel erzeugt und in self._stacks gespeichert. 
        # Jeder Stapel bekommt hierbei die entsprechende Anzahl verdeckter Karten und die eine aufgedeckte Karte uebergeben.
        self._stacks = []
        for k in range(10):
            face_down_cards = [self._stack2deal.pop() for _ in range(cards2deal_perstack[k])]
            self._stacks.append(Stack(self._stack2deal.pop(), face_down_cards))

    # TODO: Hier kommt Ihr Code

    # self. deal

    def __str__(self):
        res = ""
        for i, stack in enumerate(self._stacks):
            res += str(i) + " " + str(stack) + "\n"
        res += "#" * 80 + "\n"
        return res


if __name__ == "__main__":
    random.seed(85685679856680)
    ss = SpiderSolitaire()
    for _ in range(5):
        ss.deal()
    print(ss)
