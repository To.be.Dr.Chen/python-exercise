import random

def initialize():

    deck_of_cards = []
    deck_of_cards = [(value, color) for value in range(1, 14) for color in ['hearts', 'spades']]  ##这个语句就很方便了
    stack2deal = 4 * deck_of_cards
    random.shuffle(stack2deal)
    stacks = {k: [stack2deal.pop()] for k in range(1, 11)}
    cards2deal_perstack = [50, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4]

    return stacks, stack2deal, cards2deal_perstack


def deal(stacks, stack2deal, cards2deal_perstack):
    """
    Funktion, die das Austeilen realisiert.
    """
    if cards2deal_perstack[0] == 0:
        print('All cards have already been dealt')
    elif any(not stack for stack in stacks.values()):
        print('There must be at least one card at every stack!')
    else:
        for key in stacks:
            card = stack2deal.pop()
            last_card = stacks[key][-1][-1]
            if card[0]+1 == last_card[0] and card[1] == last_card[1]:
                stacks[key][-1].append(card)
                test_fullsequence(key, stacks, stack2deal, cards2deal_perstack)
            else:
                stacks[key].append([card])
    return stacks, stack2deal, cards2deal_perstack

def test_fullsequence(key, stacks, stack2deal, cards2deal_perstack):
    """
    Funktion, die fuer einen bestimmten Stapel prueft, ob am Ende dieses Stapels eine Sequenz
    von Koenig bis Ass vorliegt und diese vollstaendige Sequenz dann entfernt.
    Verwenden Sie diese Funktion auch an den richtigen Stellen der Funktion `deal`.
    """
    # TODO: Hier kommt ihr Code
    if len(stacks[key][-1]) == 13:
        stacks[key].pop()
        test_revealcard(ey, stacks, stack2deal, cards2deal_perstack)

def test_revealcard(key, stacks, stack2deal, cards2deal_perstack):
    """
    Prueft fuer einen bestimmten Stapel, ob am Ende dieses Stapels keine offene,
    sondern noch eine aufzudeckende Karte liegt und diese dann aufdeckt.
    Verwenden Sie diese Funktion auch an den richtigen Stellen der Funktion `test_fullsequence`.
    """
    # Diese Funktion ist bereits implementiert, sie wurde im Video bereits gezeigt. 
    if cards2deal_perstack[key] > 0 and not stacks[key]:
        stacks[key].append([stack2deal.pop()])
        cards2deal_perstack[key] -= 1
        return stacks, stack2deal, cards2deal_perstack
    else:
        pass


stacks, stack2deal, cards2deal_perstack = initialize()  
print('zhanshi:',stacks)
print('suoyou:',stack2deal)
print(cards2deal_perstack)
deal(stacks, stack2deal, cards2deal_perstack)