from spidersolitaire import Card, Sequence, Stack, SpiderSolitaire

stack = Stack(Card(2, "spades"), [])

seq = Sequence([Card(1, "hearts")])
stack._sequences.append(seq)
stack._sequences.insert(0, Sequence([Card(3, "hearts")]))
