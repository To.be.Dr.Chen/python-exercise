import numpy as np
from pylab import *
from PIL import Image

def prewittx(X):
    A = array([1,0,-1],[1,0,-1],[1,0,-1])
    print(A)
    return A
##A = array([[1,0,-1],[1,0,-1],[1,0,-1]])
##print(A)
def Imfilter(A,B):
    '''
    :param A:Image Matrix
    :param B:Filter Matrix
    :return:the Image after filter
    '''

    [row, con] = np.shape(A)
    [c,d] = np.shape(B) ##size of the mask
    t = int((c-1)/2)
    r = int((d-1)/2)
    New_A = np.zeros(A.shape)
    for i in range(row):
        for j in range(con):
            New_A[i, j] =0
            i
            a = int(i-t) ##index of the first elemet in the mask area.
            b = int(j-r)
            C = (np.linspace(a,a+c-1,c))
            D = (np.linspace(b, b+d-1, d))
            for a in C:
                for b in D:
                    if a < 0 or b < 0 or a >=row or b >=con :
                        New_A[int(i), int(j)] = New_A[int(i),int(j)] + 0 * B[int(a+t-i), int(b+r-j)]
                    else:
                        New_A[int(i), int(j)] = New_A[int(i), int(j)] + A[int(a), int(b)] * B[int(a + t - i), int(b + r - j)]
    return New_A
