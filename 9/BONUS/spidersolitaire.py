import random
from time import sleep
import sys

# Dieses dictionary dient lediglich zur Ausgabe in der Console.
# Es wird in der Klasse Card und Stack für die magische Methode '__str__' verwendet
n = [i for i in range(14) if i != 11]
uni_cards = {
    'hearts': {v+1: chr(127153 + n[v]) for v in range(13)},
    'spades': {v+1: chr(127137 + n[v]) for v in range(13)},
    'face_down': chr(127136)
}
# Falls die Kartensymbole schlecht lesbar sind:
#uni_cards = {
#    'hearts': {v+1: "H{:02}".format(v+1) for v in range(13)},
#    'spades': {v+1: "S{:02}".format(v+1) for v in range(13)},
#    'face_down': "|X|"
#}


class Card:
    """
    Eine einzelne Spielkarte, die Informationen bzgl Kartenwert und -Farbe speichert.
    """
    def __init__(self, value, color):
        self._value = value
        self._color = color
    
    def get_value(self):
        "Liefert den Wert der Karte"
        return self._value

    def get_color(self):
        "Liefert die Farbe der Karte"
        return self._color

    def fits_to(self, card, value_only=False):
        "Prueft, ob diese Karte an eine andere angehaengt werden kann"
        if value_only:
            return self._value + 1 == card.get_value() and self._color != card.get_color()
        else:
            return self._value + 1 == card.get_value() and self._color == card.get_color()

    def __str__(self):
        return uni_cards[self.get_color()][self.get_value()]


class Sequence:
    """
    Diese Klasse modelliert eine absteigende Sequenz von Karten
    """
    def __init__(self, list_of_cards):
        self._cards = list_of_cards

    def first_card(self):
        "Liefert die erste Karte dieser Sequenz"
        return self._cards[0]

    def last_card(self):
        "Liefert die letzte Karte dieser Sequenz"
        return self._cards[-1]

    def append_card(self, card):
        "Fuegt der Sequenz eine Karte hinzu"
        self._cards.append(card)

    def fits_to(self, other, value_only=False):
        "Prueft, ob diese Sequenz an eine andere angehaengt werden kann"
        return self.first_card().fits_to(other.last_card(), value_only=value_only)

    def merge(self, other):
        "Kombiniert diese Sequenz mit einer anderen, indem die andere Sequenz angehaengt wird."
        self._cards += other._cards
    
    def split(self, index):
        "Teilt diese Sequenz am gegebenen Index und liefert eine neue Sequenz mit den abgetrennten Karten."
        splitted = Sequence(self._cards[index:])
        self._cards[:] = self._cards[:index]
        return splitted

    def is_full(self):
        "Prueft, ob die Sequenz vollstaendig ist, also alle 13 Karten beinhaltet."
        return len(self._cards) == 13

    # TODO: Hier kommt Ihr Code    
    
    def __str__(self):
        return "-".join(map(str, self._cards))


class Stack:
    """
    Ein Stapel von Sequenzen. Diese Klasse modelliert die einzelnen Stapel des Spiels.
    Neben den Sequenzen, welche den aufgedeckten Karten entsprechen, merkt sich ein Stapel noch die umgedrehten/verdeckten Karten.
    """
    def __init__(self, card, face_down_cards):
        self._sequences = [Sequence([card])]
        self._face_down_cards = face_down_cards

    def last_sequence(self):
        "Liefert die letzte Sequenz in diesem Stapel"
        return self._sequences[-1]

    def is_empty(self):
        "Prueft, ob dieser Stapel leer ist, es also keine offenen Karten mehr gibt."
        return not self._sequences
    
    def append_sequence(self, seq):
        "Fuegt dem Stapel eine Sequenz hinzu"
        self._sequences.append(seq)
    
    # TODO: Hier kommt Ihr Code


    def test_fullsequence(self):
        "Prueft, ob die letzte Sequenz vollstaendig ist und deckt in diesem Fall eine neue Karte auf."
        if self.last_sequence().is_full():
            self._sequences.pop()
            self.test_revealcard()

    def test_revealcard(self):
        """
        Deckt, wenn moeglich, eine neue Karte von den zugedeckten Karten auf.
        Dafuer muss der Stapel leer sein und es muss noch zugedeckte geben.
        """
        if self.is_empty() and self._face_down_cards:
            self.append_sequence(Sequence([self._face_down_cards.pop()]))
    
    def deal_card(self, card):
        """
        Realisiert das Austeilen einer Karte auf den Stapel.
        Die Karte wird entweder an die untersten Sequenz angehaengt oder es wird eine neue erzeugt.
        Im ersten Fall kann eine vollstaendige Sequenz entstehen und muss deshalb durch 'test_fullsequence()' ueberprueft werden.
        """
        last_card = self.last_sequence().last_card()
        if card.fits_to(last_card):
            self.last_sequence().append_card(card)
            self.test_fullsequence()
        else:
            self.append_sequence(Sequence([card]))

    def __str__(self):
        return " ".join(len(self._face_down_cards) *  [uni_cards['face_down']] + list(map(str, self._sequences)))
       


class SpiderSolitaire:
    """
    Klasse, die das ganze Spielfeld an sich verwaltet.
    """
    def __init__(self):
        # wir starten mit allen Karten (4 ganze Kartendecks mit jeweils 13 Herz und 13 Pik)
        self._stack2deal = 4*[Card(value, color) for value in range(1, 14) for color in ["hearts", "spades"]]
        # Durchmischen aller Karten
        random.shuffle(self._stack2deal)

        # Anzahl verdeckter Karten pro Stapel
        cards2deal_perstack = [5, 5, 5, 5, 4, 4, 4, 4, 4, 4]

        # Es werden 10 Stapel erzeugt und in self._stacks gespeichert. 
        # Jeder Stapel bekommt hierbei die entsprechende Anzahl verdeckter Karten und die eine aufgedeckte Karte uebergeben.
        self._stacks = []
        for k in range(10):
            face_down_cards = [self._stack2deal.pop() for _ in range(cards2deal_perstack[k])]
            self._stacks.append(Stack(self._stack2deal.pop(), face_down_cards))
        
        # Sequenz unter dem Mauszeiger/bewegende Sequenz
        self.moving_sequence = None
        # Woher kam die bewegte Sequenz
        self.origin_stack_index = None

    def deal(self):
        """
        Teilt an jeden der 10 Stapel eine Karte aus.
        Vorher muss geprueft werden, ob es noch Karten zum austeilen gibt und auf jedem Stapel mindestens eine aufgedeckte Karte liegt.
        """
        if not self._stack2deal:
            print("All cards have already been dealt")
            return

        if any(stack.is_empty() for stack in self._stacks):
            print("There must be at least one card at every stack!")
            return

        for stack in self._stacks:
            stack.deal_card(self._stack2deal.pop())
    
    def pick_up(self, stack_index, card_index):
        """
        "Aufheben" einer Sequenz
        """
        if self.moving_sequence is not None:
            print("Already moving!", file=sys.stderr)
            return
        
        if not (0 <= stack_index < 10):
            print("Wrong index for stack!", file=sys.stderr)
            return
        
        # TODO: Hier kommt Ihr Code

    
    def move(self, stack_index):
        if self.moving_sequence is None:
            print("There is nothing to move. Call 'pick_up' first.", file=sys.stderr)
            return
        
        if not (0 <= stack_index < 10) or stack_index == self.origin_stack_index:
            print("Wrong index for stack", file=sys.stderr)
            return

        # TODO: Hier kommt Ihr Code
        

    def play(self):
        # Wir sind gerade dabei eine Sequenz zu bewegen
        if self.moving_sequence is not None:
            print("picked up: " + str(self.moving_sequence))
            print("Options:")
            print("k    move sequence to stack k")
            print('"b"  move sequence back to original stack {}'.format(self.origin_stack_index))
            
            user_in = input("Input: ").strip().lower()
            
            # Zuruecklegen auf den ursprungsstapel (Abbruch)
            if user_in == "b":
                self._stacks[self.origin_stack_index].last_sequence().merge(self.moving_sequence)
                self.moving_sequence = None
                self.origin_stack_index = None
                return
            
            try:
                stack_index = int(user_in)
            except ValueError:
                print("Wrong input!", file=sys.stderr)
                return
                
            self.move(stack_index)
            
            # Gewinnabfrage
            if all(stack.is_empty() for stack in self._stacks):
                print("Congratulations, you won!")
                return True
        else:
            print("Options:")
            # es gibt noch Karten zum Austeilen
            if self._stack2deal:
                print('"d"   deal (there are still {} cards to deal)'.format(len(self._stack2deal)))                
            print("k, n  pick up the last subsequence (part [n:]) of stack k")
            print("k     pick up the last sequence of stack k")
            user_in = input("Input: ").strip().lower()
        
            # Austeilen
            if user_in == "d" :
                self.deal()
                return
        
            try:
                splitted = user_in.split(",")
                # nur Stacknummer eingegeben
                if len(splitted) == 1:
                    stack_index, card_index = int(splitted[0]), 0
                # beides eingegeben
                else:
                    stack_index, card_index = map(int, splitted)
            except ValueError:
                print("Wrong input!", file=sys.stderr)
                return
            
            self.pick_up(stack_index, card_index)

    def __str__(self):
        res = ""
        for i, stack in enumerate(self._stacks):
            res += str(i) + str(stack) + "\n"
        return res


if __name__ == "__main__":
    random.seed(85685679856680)
    ss = SpiderSolitaire()
    
    is_won = False
    while not is_won:
        print(ss)
        is_won = ss.play()
        sleep(0.5)
