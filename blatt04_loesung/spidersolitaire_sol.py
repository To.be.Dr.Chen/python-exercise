import random

def initialize():
    """
    Diese Funktion soll die Anfangskonfiguration des Spiels herstellen.
    Rueckgabewerte dieser Funktion sind fuer die auf dem Aufgabenblatt vorgeschlagene Datenstruktur 
    das Dictionary stacks, die Liste stack2deal und die Liste cards2deal_perstack.
    """
    stack2deal = 4*[(number, color) for number in range(1, 14) for color in ["hearts", "spades"]]
    random.shuffle(stack2deal)
    stacks = {k:[[stack2deal.pop()]] for k in range(1, 11)}
    cards2deal_perstack = [50, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4]
    return stacks, stack2deal, cards2deal_perstack   
    

def deal(stacks, stack2deal, cards2deal_perstack):
    """
    Funktion, die das Austeilen realisiert.
    """
    if cards2deal_perstack[0] == 0:
        print("All cards have already been dealt")
    else:
        for key in stacks:
            card = stack2deal.pop()
            last_card = stacks[key][-1][-1]
            if card[0]+1 == last_card[0] and card[1] == last_card[1]:
                stacks[key][-1].append(card)
                test_fullsequence(key, stacks, stack2deal, cards2deal_perstack)
            else:
                stacks[key].append([card])
        cards2deal_perstack[0] -= 10
        
        
def test_fullsequence(key, stacks, stack2deal, cards2deal_perstack):
    """
    Funktion, die fuer einen bestimmten Stapel prueft, ob am Ende dieses Stapels eine Sequenz
    von Koenig bis Ass vorliegt und diese vollstaendige Sequenz dann entfernt.
    Verwenden Sie diese Funktion auch an den richtigen Stellen der Funktion `deal`.
    """
    if len(stacks[key][-1]) == 13:
        stacks[key].pop()
        test_revealcard(key, stacks, stack2deal, cards2deal_perstack)


def test_revealcard(key, stacks, stack2deal, cards2deal_perstack):
    print("reveal")
    """
    Prueft fuer einen bestimmten Stapel, ob am Ende dieses Stapels keine offene,
    sondern noch eine aufzudeckende Karte liegt und diese dann aufdeckt.
    Verwenden Sie diese Funktion auch an den richtigen Stellen der Funktion `test_fullsequence`.
    """
    if  cards2deal_perstack[key] > 0 and not stacks[key]:
        stacks[key].append([stack2deal.pop()])
        cards2deal_perstack[key] -= 1


stacks, stack2deal, cards2deal_perstack = initialize()
deal(stacks, stack2deal, cards2deal_perstack)
